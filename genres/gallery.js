// Code for Modal:

const lightbox = document.createElement('div')
// const lightboxCaption = document.createElement('div')
lightbox.id = 'lightbox'
// lightboxCaption.id = 'lightboxCaption'
document.body.appendChild(lightbox)
// document.body.appendChild(lightboxCaption)

const images = document.querySelectorAll('.genre img')
images.forEach(image => {
  image.addEventListener('click', e => {
    // const para = document.createElement ('p') 
    // para.innerHTML=e.target.alt;
    lightbox.classList.add('active')
    // lightboxCaption.classList.add('active')
    const img = document.createElement('img')
    img.src = image.src
    while (lightbox.firstChild) {
      lightbox.removeChild(lightbox.firstChild)
      // lightboxCaption.removeChild(lightboxCaption.firstChild)
    }

    lightbox.appendChild(img)
    // lightboxCaption.appendChild(para)

  })
})

lightbox.addEventListener('click', e => {
  if (e.target !== e.currentTarget) return
  lightbox.classList.remove('active')
  // lightboxCaption.classList.remove('active')
})




// Code for Music Button:

  var aud = document.getElementById("ASong").children[0];
  var isPlaying = false;
  aud.pause();

  function playPause() {
    if (isPlaying) {
      aud.pause();
    } else {
      aud.play();
    }
    isPlaying = !isPlaying;
  }
  